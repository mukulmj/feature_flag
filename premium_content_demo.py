class PremiumContentApp:
    def __init__(self):
        self.feature_flags = {
            'premium_content': False  # Default: Premium Content feature is disabled
        }

    def enable_feature(self, feature_name):
        self.feature_flags[feature_name] = True

    def disable_feature(self, feature_name):
        self.feature_flags[feature_name] = False

    def show_content(self):
        if self.feature_flags['premium_content']:
            print("Welcome to Premium Content!")
            print("Here's some exclusive content for premium users.")
        else:
            print("Welcome! Check out our regular content.")

if __name__ == "__main__":
    app = PremiumContentApp()

    while True:
        print("\nMenu:")
        print("1. Enable Premium Content Feature")
        print("2. Disable Premium Content Feature")
        print("3. Show Content")
        print("4. Exit")

        choice = input("Enter your choice: ")

        if choice == '1':
            app.enable_feature('premium_content')
            print("Premium Content feature enabled.")
        elif choice == '2':
            app.disable_feature('premium_content')
            print("Premium Content feature disabled.")
        elif choice == '3':
            app.show_content()
        elif choice == '4':
            print("Exiting.")
            break
        else:
            print("Invalid choice. Please select a valid option.")
