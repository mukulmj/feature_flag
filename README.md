# ShareChat Staging Environment Setup Guide

---

## 1. Introduction

This document provides a step-by-step guide to creating a staging environment for ShareChat using Google Cloud Platform. The environment will be configured with specific settings to ensure optimal performance and security.

## 2. Required Permissions

Before starting the environment setup, ensure the following permissions are granted to the relevant users, service accounts, and principals:

| Project | Permission | Purpose |
| --- | --- | --- |
| prj-sc-s-shared-network | (service-project-id)@cloudservices.gserviceaccount.com | | Compute Network User |
| prj-sc-s-shared-network | (service-service-project-id)@cloudcomposer-accounts.iam.gserviceaccount.com | | Composer Shared VPC Agent |
| prj-sc-p-shared-network | (service-project-id)@cloudservices.gserviceaccount.com | | Compute Network User |
| prj-sc-p-shared-network | (service-service-project-id)@cloudcomposer-accounts.iam.gserviceaccount.com | | Composer Shared VPC Agent |

**Note: service-project-id = id of the project in which compser is being created**

## 3. Environment Setup Steps

### Step 1: Access IAM & Admin Console

1. Log in to the Google Cloud Console.
2. Navigate to the "IAM & Admin" section in the network project.

### Step 2: Grant Access to the New Principal

1. Under IAM, click on "Add" to add a new member.
2. Fill in the necessary details for the new principal as shown in the provided image.
3. Assign the required roles to the principal as mentioned in the above table.

### Step 3: Service Account Permissions

1. Grant the "Shared-VPC-IAM" permission to manage IAM roles within the shared VPC.
2. Ensure that the service account "service-<project-id>@cloudcomposer-accounts.iam.gserviceaccount.com" has the necessary permissions to access Composer accounts.
3. Grant the "Composer Shared VPC Agent" permission as required.

### Step 4: Environment Configuration

1. In the Composer section, select "Composer-02" as the environment type.
2. Click on the "Create" button to proceed.
3. On the next page, provide the required details:
    
    
    | Name | composer name |
    | --- | --- |
    | Location | region |
    | Image Version | default |
    | Service Account | default |
4. Check the box to grant necessary permissions as shown in the provided screenshot.

![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled.png)

### Step 5: Configure Resilience Mode

1. Choose "Standard Resilience" mode.
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%201.png)
    
2. Select "Medium" for Environment Resources.

![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%202.png)

### Step 6: Network Configuration

1. Choose "**Networks shared with me (from the host project: <project name>)”**.
2. Fill in the **Network** and **Subnetwork** details.
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%203.png)
    
3. Configure **“secondary IP ranges for pods”**.
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%204.png)
    
4. Configure **“secondary IP ranges for services”**.
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%205.png)
    
5. Choose **"Private IP Environment"** for the **Networking Type**.
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%206.png)
    
6. Select the appropriate settings for **“Composer connectivity”** and select ******************************************************“Private Service Connect (default)”******************************************************
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%207.png)
    
7. Select the appropriate settings for **“Composer connection subnetwork”** and select **“Subnetwork”**
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%208.png)
    
8. Configure the **“IP range for the GKE control plane network”** and select **“Custom IP range”**
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%209.png)
    
9. For "**Access to Cluster Endpoints**" select "**Enable access to public endpoint from authorized networks (default)”**.
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%2010.png)
    
10. In "**Web server network access control**" choose "**Allow access from all IP addresses**"
    
    ![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%2011.png)
    

![Untitled](ShareChat%20Staging%20Environment%20Setup%20Guide%20391bc48072384cbe8c7fe98f300dd844/Untitled%2012.png)

### Step 7: Finalize and Create

1. Review the configured settings.
2. Click on "Create" to initiate the ShareChat staging environment creation process.

## 4. Conclusion

This document has provided a detailed guide to setting up the ShareChat staging environment using the Google Cloud Platform. Follow the steps accurately and ensure all necessary permissions are granted for a successful setup. Refer to the provided images for visual assistance during the process.
