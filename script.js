// Replace with your actual GitLab project ID and feature flag name
const projectId = '48747962';
const featureFlagName = 'test-feature';
const accessToken = 'glpat-Axx--SJ6Miv6DWEBtSwa';

const apiUrl = `https://gitlab.com/api/v4/projects/${projectId}/feature_flags/${featureFlagName}`;

async function getFeatureFlagState() {
  try {
    const response = await fetch(apiUrl, {
      headers: {
        'PRIVATE-TOKEN': accessToken
      }
    });
    const data = await response.json();
    return data.enabled;
  } catch (error) {
    console.error('Error fetching feature flag state:', error);
    return false;
  }
}

async function updatePageColor() {
  const isEnabled = await getFeatureFlagState();
  document.body.style.backgroundColor = isEnabled ? 'white' : 'black';
}

updatePageColor();
